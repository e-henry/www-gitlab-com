---
layout: markdown_page
title: Team Handbook
twitter_image: '/images/tweets/handbook-gitlab.png'
---


The GitLab team handbook is the central repository for how we run the company. As part of our dedication to being as open and transparent as possible, the handbook is open to the world, and we welcome feedback<a name="feedback"></a>. Please make a <a href="https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests">merge request</a> to suggest improvements or add clarifications.
Please use <a href="https://gitlab.com/gitlab-com/www-gitlab-com/issues">issues</a> to ask questions.

* General
  * [Values](/handbook/values)
  * [General Guidelines](/handbook/general-guidelines)
  * [Handbook Usage](/handbook/handbook-usage)
  * [Communication](/handbook/communication)
  * [Spending Company Money](/handbook/spending-company-money)
  * [Paid time off](/handbook/paid-time-off)
  * [Incentives](/handbook/incentives)
  * [Signing legal documents](/handbook/signing-legal-documents)
  * [Working remotely](/handbook/working-remotely)
  * [Tools and tips](/handbook/tools-and-tips)
  * [Using Git to update this website](/handbook/git-page-update)
  * [Secret Santa](/handbook/secret-santa)
* [Engineering](/handbook/engineering)
  * [Support](/handbook/support)
  * [Infrastructure](/handbook/infrastructure)
  * [Backend](/handbook/backend)
  * [Edge](/handbook/edge)
  * [UX](/handbook/ux)
  * [Build](/handbook/build)
* [Marketing](/handbook/marketing)
  * [Content Team](/handbook/marketing/content/)
  * [Blog](/handbook/marketing/blog)
  * [Markdown Guide](/handbook/marketing/developer-relations/technical-writing/markdown-guide/)
  * [Social Marketing](/handbook/marketing/social-marketing/)
  * [Social Media Guidelines](/handbook/marketing/social-media-guidelines)
* [Sales](/handbook/sales)
  * [Customer Success](/handbook/customer-success)
* [Finance](/handbook/finance)
  * [Stock Options](/handbook/stock-options)
  * [Board meetings](/handbook/board-meetings)
* [People Operations](/handbook/people-operations)
  * [Onboarding](/handbook/general-onboarding)
  * [Benefits](/handbook/benefits)
  * [Hiring](/handbook/hiring)
  * [Travel](/handbook/travel)
  * [Security](/handbook/security)
  * [Leadership](/handbook/leadership)
* [Product](/handbook/product)
  * [Making Gifs](/handbook/product/making-gifs)
  * [Product areas](/handbook/product/product-areas)
  * [Data analysis](/handbook/product/data-analysis)

[async-communication]: https://about.gitlab.com/2016/03/23/remote-communication#asynchronous-communication-so-everyone-can-focus
[check-my-links]: https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf/
[kramdown]: http://kramdown.gettalong.org/
[markdown guide]: /handbook/marketing/developer-relations/technical-writing/markdown-guide/


<style>
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
</style>
